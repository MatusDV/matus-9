#pragma once
#include <iostream>

using namespace std;

void ProductsAfterMaxElements(const int* const* const matrix, const unsigned int matrixSize) {
	int maxElement = 0;
	int maxElementIndex = 0;

	for (unsigned int row = 0; row < matrixSize; row++) {
		maxElement = matrix[row][0];
		for (unsigned int column = 0; column < matrixSize; column++) {
			if (matrix[row][column] > maxElement) {
				maxElement = matrix[row][column];
				maxElementIndex = column;
			}
		}
		cout << "Max element on the " << row << "-th line: " << maxElement << endl;
		int product = 1;
		for (unsigned int column = maxElementIndex + 1; column < matrixSize; column++) {
			product *= matrix[row][column];
		}
		cout << "The product of the elements: " << product << endl;
	}
}

void MatIsSymmetrical(const int* const* const matrix, const unsigned int matrixSize) {
	for (unsigned int row = 0; row < matrixSize; row++) {
		for (unsigned int column = 0; column < matrixSize / 2 - 1; column++) {
			if (matrix[row][column] != matrix[row][matrixSize + 1 - column]) {
				cout << "The matrix is not symmetrical" << endl;
				return;
			}
		}
	}
	cout << "The matrix is symmetrical" << endl;
}

void MatHalfSum(const int* const* const matrix, const unsigned int matrixSize) {
	int sum = 0;

	for (unsigned int row = 0; row < matrixSize - 1; row++) {
		for (unsigned int column = matrixSize + 1 - row; column < matrixSize; column++) {
			sum += matrix[row][column];
		}
	}
	cout << "Sum: " << sum;
}
