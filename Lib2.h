#pragma once
#include <iostream>
using namespace std;

void GenerateMatrix(int** const matrix, const unsigned int matrixSize) {
	for (unsigned int row = 0; row < matrixSize; row++) {
		for (unsigned int column = 0; column < matrixSize; column++) {
			matrix[row][column] = (rand() % 19) - 9;
		}
	}
}

void PrintMatrix(const int* const* const matrix, const unsigned int matrixSize) {
	for (unsigned int row = 0; row < matrixSize; row++) {
		for (unsigned int column = 0; column < matrixSize; column++) {
			cout << matrix[row][column] << " ";
		}
		cout << endl;
	}
}
