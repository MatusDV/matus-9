#pragma once
#include <iostream>
#include <string>
#include <WinBase.h>
#include <consoleapi2.h>
#include <conio.h>

using std::cin;
using std::cout;
using std::endl;
using std::rand;
using std::srand;
using std::string;
const unsigned int KEY_UP = 72;
const unsigned int KEY_DOWN = 80;
const unsigned int KEY_ENTER = 13;
const unsigned int KEY_ESC = 27;
const string MENU[5] = { "Print matrix", "Product of elements located after the maximum element in a row, for each row",
  "Check whether all rows are symmetrical relative to the middle", "Calculate the sum of matrix elements highlighted in black", "Exit" };
const unsigned int TEXT_COLOR = 3;
const unsigned int TEXT_SELECTED_COLOR = 9;
const unsigned int MENU_SIZE = 5;
const unsigned int MENU_PRINT = 0;
const unsigned int MENU_PRODUCT = 1;
const unsigned int MENU_SYMMETRY = 2;
const unsigned int MENU_SUM = 3;
const unsigned int MENU_EXIT = 4;
const unsigned int MENU_BACK = -1;
const HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

void ClearScreen() {
	COORD tl = { 0,0 };
	CONSOLE_SCREEN_BUFFER_INFO s;
	GetConsoleScreenBufferInfo(consoleHandle, &s);
	DWORD written, cells = s.dwSize.X * s.dwSize.Y;
	FillConsoleOutputCharacter(consoleHandle, ' ', cells, tl, &written);
	FillConsoleOutputAttribute(consoleHandle, s.wAttributes, cells, tl, &written);
	SetConsoleCursorPosition(consoleHandle, tl);
}

void DisplayMenu(const string* const menuItems, const unsigned int menuSize, const unsigned int selectedItem) {
	ClearScreen();
	SetConsoleTextAttribute(consoleHandle, TEXT_COLOR);
	for (unsigned int menuItem = 0; menuItem < menuSize; menuItem++) {
		if (menuItem == selectedItem) {
			SetConsoleTextAttribute(consoleHandle, TEXT_SELECTED_COLOR);
			cout << menuItems[menuItem] << endl;
			SetConsoleTextAttribute(consoleHandle, TEXT_COLOR);
		}
		else {
			cout << menuItems[menuItem] << endl;
		}
	}
}
unsigned int GetSelectedItem(const string* const menuItems, const unsigned int menuSize) {
	unsigned int menuItem = 0;
	unsigned int pressedKey = 0;

	while (true) {
		DisplayMenu(menuItems, menuSize, menuItem);
		pressedKey = _getch();
		switch (pressedKey) {
		case KEY_UP:
		{
			if (menuItem <= 0) {
				menuItem = menuSize - 1;
			}
			else {
				menuItem--;
			}
			break;
		}
		case KEY_DOWN:
		{
			if (menuItem >= menuSize - 1) {
				menuItem = 0;
			}
			else {
				menuItem++;
			}
			break;
		}
		case KEY_ENTER:
		{
			return menuItem;
		}
		case KEY_ESC:
		{
			return MENU_BACK;
		}
		}
	}
}
