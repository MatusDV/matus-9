﻿#include <iostream>
#include <string>
#include <windows.h>
#include <conio.h>
#include <ctime>
#include "Lib1.h"
#include "Lib2.h"
#include "Lib3.h"

using namespace std;

int main()
{
	int** matrix;
	unsigned int matrixSize = 0;

	SetConsoleTextAttribute(consoleHandle, TEXT_COLOR);
	cout << "Enter the matrix size:" << endl;
	cin >> matrixSize;
	matrix = new int* [matrixSize];
	for (unsigned int row = 0; row < matrixSize; row++) {
		matrix[row] = new int[matrixSize];
	}
	srand(time(0));
	GenerateMatrix(matrix, matrixSize);
	while (true) {
		unsigned int menuItem = GetSelectedItem(MENU, MENU_SIZE);
		ClearScreen();
		switch (menuItem) {
		case MENU_PRINT: {
			PrintMatrix(matrix, matrixSize);
			_getch();
			break;
		}
		case MENU_PRODUCT: {
			ProductsAfterMaxElements(matrix, matrixSize);
			_getch();
			break;
		}
		case MENU_SYMMETRY: {
			MatIsSymmetrical(matrix, matrixSize);
			_getch();
			break;
		}
		case MENU_SUM: {
			MatHalfSum(matrix, matrixSize);
			_getch();
			break;
		}
		case MENU_EXIT: {
			break;
		}
		case MENU_BACK: {
			break;
		}
		}
		if (menuItem == MENU_EXIT || menuItem == MENU_BACK) {
			break;
		}
	}
	for (unsigned int row = 0; row < matrixSize; row++) {
		delete[] matrix[row];
	}
	delete[] matrix;
}
